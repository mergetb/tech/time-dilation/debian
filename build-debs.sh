#!/bin/bash

set -e

cp kconfig-generic kvm/.config
cd kvm
make deb-pkg -j`nproc`

