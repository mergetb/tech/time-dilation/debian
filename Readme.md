# Debian Time Dilation Packages

This repository contains scripts and CI automation for creating debian packages
for the Merge time dilation technologies. These include

- kvm: a variant of the Linux kernel that contains a modified KVM module that
  knows how to do time dilation.
- qemu: a variant of QEMU that knows how to do time dilation in conjunction with
  our KVM variant.

The above projects are imported as submodules.
